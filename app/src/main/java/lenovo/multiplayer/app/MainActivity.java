package lenovo.multiplayer.app;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;

import lenovo.multiplayer.advert.AdConstant;
import lenovo.multiplayer.advert.AdUtil;

public class MainActivity extends Activity implements AdConstant {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        MultiVideoPlayer multiPlayer = findViewById(R.id.multi_player);
//
//        multiPlayer.setListener(new MultiVideoPlayer.MultiVideoListener() {
//            @Override
//            public void onStartPlay(int pos) {
//                Log.d(TAG, "onStartPlay() called with: pos = [" + pos + "]");
//            }
//
//            @Override
//            public void onCompletion(int pos) {
//                Log.d(TAG, "onCompletion() called with: pos = [" + pos + "]");
//            }
//
//            @Override
//            public void onError(int pos, int what) {
//                Log.d(TAG, "onError() called with: pos = [" + pos + "], what = [" + what + "]");
//            }
//        });
//
//        multiPlayer.play(
//                "/data/advert/0_15_20181122161308-19263158.mp4",
//                "/data/advert/0_15_20181129160642-10307566.mp4"
//        );
        AdUtil.showAdvert(this, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }
}
