package lenovo.multiplayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.Arrays;


/**
 * @author tengxp
 */
public class MultiVideoPlayer extends SurfaceView {

    private static final String TAG = MultiVideoPlayer.class.getSimpleName();

    public static final int MSG_CREATE = 0x221;
    public static final int MSG_RELEASE = 0x222;
    public static final int MSG_PREPARED = 0x223;
    public static final int MSG_COMPLETION = 0x224;
    public static final int MSG_ERROR = 0x225;
    public static final int MSG_START = 0x229;

    private SurfaceHolder mHolder = null;
    private Context mContext;

    public Handler mHandler;

    private int mCurrentPos, mNextPos;
    private String[] mPathArray;

    private MediaPlayer mCurrentPlayer;
    private MediaPlayer mNextPlayer;

    private boolean isNextPrepared;

    public interface MultiVideoListener {
        void onStartPlay(int pos);

        void onCompletion(int pos);

        void onError(int pos, int what);
    }

    private MultiVideoListener mMultiVideoListener;

    public String msg2String(int msg, Object obj) {
        String txt;
        switch (msg) {
            case MSG_CREATE:
                txt = "CREATE PLAYER";
                break;

            case MSG_PREPARED:
                txt = "PREPARED PLAYER";
                break;

            case MSG_START:
                txt = "START PLAYER";
                break;

            case MSG_ERROR:
                txt = "ERROR PLAYER";
                break;

            case MSG_COMPLETION:
                txt = "COMPLETION PLAYER";
                break;

            case MSG_RELEASE:
                txt = "RELEASE PLAYER";
                break;

            default:
                txt = Integer.toString(msg);
                break;
        }
        return txt + " : " + player2String(obj);
    }

    private String player2String(Object obj) {
        if (obj == null) {
            return "null";
        } else {
            String objStr = obj.toString();
            return (mCurrentPlayer == null ? "UNKNOWN" : obj == mCurrentPlayer ? "CURRENT" : "NEXT")
                    + " " + objStr.substring(objStr.indexOf("@") + 1);
        }
    }

    public MultiVideoPlayer(Context context) {
        super(context);

        init(context);
    }

    public MultiVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public void play(String... pathArray) {
        int len = pathArray == null ? 0 : pathArray.length;
        if (len > 0) {
            mPathArray = pathArray;
            mCurrentPos = 0;

            Log.d(TAG, "play() called with: pathArray = " + Arrays.toString(pathArray));
            mHandler.obtainMessage(MSG_CREATE).sendToTarget();
        }
    }

    public void release() {
        mHandler.sendEmptyMessage(MSG_RELEASE);
    }

    public void setListener(MultiVideoListener listener) {
        this.mMultiVideoListener = listener;
    }

    public int getCurrentPosition() {
        int position = 0;
        try {
            position = mCurrentPlayer != null && mCurrentPlayer.isPlaying() ? mCurrentPlayer.getCurrentPosition() : 0;
        } catch (Exception e) {
            Log.w(TAG, "getCurrentPosition: player = " + mCurrentPlayer, e);
        }
        return position;
    }

    public int getDuration() {
        int position = 0;
        try {
            position = mCurrentPlayer != null && mCurrentPlayer.isPlaying() ? mCurrentPlayer.getDuration() : 0;
        } catch (Exception e) {
            Log.w(TAG, "getDuration: player = " + mCurrentPlayer, e);
        }
        return position;
    }

    private void init(Context context) {
        this.mContext = context;
        getHolder().addCallback(mSurfaceCallback);

        HandlerThread thread = new HandlerThread("VideoPlayer");
        thread.start();
        mHandler = new Handler(thread.getLooper(), mVideoCallback);
    }

    private boolean hasNext() {
        return mCurrentPos < mNextPos && mNextPos < mPathArray.length;
    }

    private SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mHolder = holder;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    };

    private interface Callback {
        void onPlayerCreate(MediaPlayer player, int pos);
    }

    private Handler.Callback mVideoCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(final Message msg) {
            final int what = msg.what;
            final MediaPlayer player = (MediaPlayer) msg.obj;
            String text = "handleMessage() called with: msg = [" + msg2String(what, player) + "]";
            Log.i(TAG, text);

            mHandler.removeMessages(what);

            final boolean isCurrent = mCurrentPlayer == player;
            try {
                switch (what) {
                    case MSG_CREATE:
                        preparePlayer(0, null, new Callback() {
                            @Override
                            public void onPlayerCreate(MediaPlayer player, int pos) {
                                mCurrentPlayer = player;
                                mCurrentPos = pos;
                            }
                        });

                        mNextPos = mCurrentPos + 1;

                        // if no valid video path
                        if (mCurrentPos == mPathArray.length) {
                            mHandler.sendEmptyMessage(MSG_COMPLETION);
                        }
                        break;

                    case MSG_PREPARED:
                        Log.d(TAG, String.format("handleMessage: cur = %s, next = %s", player2String(mCurrentPlayer), player2String(mNextPlayer)));
                        if (isCurrent) {
                            mHandler.obtainMessage(MSG_START, mCurrentPlayer).sendToTarget();
                        } else {
                            isNextPrepared = mNextPlayer == player;
                        }
                        break;

                    case MSG_START:
                        if (mHolder == null) {
                            // wait SurfaceHolder create
                            mHandler.sendMessageDelayed(Message.obtain(msg), 500);
                            Log.d(TAG, "handleMessage: SurfaceHolder not ready, wait 500 ms.");
                        } else if (isCurrent) {
                            mCurrentPlayer.setDisplay(mHolder);
                            mCurrentPlayer.setScreenOnWhilePlaying(true);
                            if (mMultiVideoListener != null) {
                                mMultiVideoListener.onStartPlay(mCurrentPos);
                            }
                            Log.d(TAG, text + "------------Before player.start()------------");
                            mCurrentPlayer.start();
                            Log.d(TAG, text + "------------After player.start()------------");
                            float volume = .02f;
                            mCurrentPlayer.setVolume(volume, volume);

                            Log.e(TAG, String.format("handleMessage: cur = %d, next = %d", mCurrentPos, mNextPos));
                            // check next player
                            if (hasNext()) {
                                preparePlayer(mNextPos, mNextPlayer, new Callback() {
                                    @Override
                                    public void onPlayerCreate(MediaPlayer player, int pos) {
                                        mNextPlayer = player;
                                        mNextPos = pos;
                                    }
                                });
                            }
                        }
                        break;

                    case MSG_ERROR:
                    case MSG_COMPLETION:
                        if (mMultiVideoListener != null) {
                            if (what == MSG_ERROR) {
                                int pos = isCurrent ? mCurrentPos : player == mNextPlayer ? mNextPos : -1;
                                mMultiVideoListener.onError(pos, msg.arg1);
                            } else if (isCurrent) {
                                mMultiVideoListener.onCompletion(mCurrentPos);
                            }
                        }

                        if (hasNext()) {
                            mCurrentPos = mNextPos;

                            Log.d(TAG, "handleMessage: isNextPrepared = " + isNextPrepared + ", next = " + mNextPlayer);
                            player.setDisplay(null);
                            mCurrentPlayer = mNextPlayer;
                            mNextPlayer = player;

                            if (isNextPrepared) {
                                mHandler.obtainMessage(MSG_START, mCurrentPlayer).sendToTarget();
                            }
                        } else if (player != null) {
                            Log.d(TAG, "handleMessage: release " + player2String(player));
                            mHandler.obtainMessage(MSG_RELEASE, player).sendToTarget();
                        }
                        break;

                    case MSG_RELEASE:
                        if (player == null) {
                            // user stop player, so destroy players
                            destroyPlayer(mCurrentPlayer);
                            destroyPlayer(mNextPlayer);
                        } else {
                            destroyPlayer(player);
                        }
                        break;

                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        private void destroyPlayer(MediaPlayer player) {
            if (player == null) {
                return;
            }
            try {
                player.stop();
            } catch (Exception e) {
                Log.w(TAG, "destroyPlayer: " + player, e);
            }
            try {
                player.reset();
            } catch (Exception e) {
                Log.w(TAG, "destroyPlayer: " + player, e);
            }
            player.release();

            if (player == mCurrentPlayer) {
                mCurrentPlayer = null;
            } else if (player == mNextPlayer) {
                mNextPlayer = null;
            }
        }

        private void preparePlayer(int start, MediaPlayer player, Callback callback) {
            int len = mPathArray.length;
            MediaPlayer mp = null;
            for (int i = start; i < len; i++) {
                mp = createPlayer(mPathArray[i], player);
                if (mp != null) {
                    callback.onPlayerCreate(mp, i);
                    break;
                }
            }

            if (mp == null) {
                callback.onPlayerCreate(null, len);
            }
        }

        private MediaPlayer createPlayer(String videoPath, MediaPlayer player) {
            if (player == null) {
                player = new MediaPlayer();

                VideoListener listener = new VideoListener();
                player.setOnPreparedListener(listener);
                player.setOnErrorListener(listener);
                player.setOnCompletionListener(listener);
                player.setOnInfoListener(listener);
                player.setOnSeekCompleteListener(listener);
                player.setOnVideoSizeChangedListener(listener);
                player.setOnBufferingUpdateListener(listener);
            } else {
                player.reset();
            }

            Log.d(TAG, "createPlayer() called with: videoPath = [" + videoPath + "]");

            MediaPlayer mp = player;
            try {
                mp.setDataSource(videoPath);
                mp.setLooping(false);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
                mHandler.obtainMessage(MSG_RELEASE, mp).sendToTarget();
                mp = null;
            }

            return mp;
        }
    };

    private class VideoListener implements MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener,
            MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener,
            MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnVideoSizeChangedListener {

        @Override
        public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onBufferingUpdate : player = " + mediaPlayer + ", arg2 = " + i + sn);
        }

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onCompletion : player = " + mediaPlayer + sn);
            mHandler.obtainMessage(MSG_COMPLETION, mediaPlayer).sendToTarget();
        }

        @Override
        public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onError() called with: player = [" + mediaPlayer
                    + "], what = [" + what + "], extra = [" + extra + "]" + sn);
//            int pos = mediaPlayer == mCurrentPlayer ? mCurrentPos : mediaPlayer == mNextPlayer ? mNextPos : -1;
//            if (pos != -1 && mMultiVideoListener != null) {
//                mMultiVideoListener.onError(pos, what);
//            }
//            mHandler.obtainMessage(MSG_ERROR, what, extra, mediaPlayer).sendToTarget();

            return false;
        }

        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onPrepared : player = " + mediaPlayer + sn);
            mHandler.obtainMessage(MSG_PREPARED, mediaPlayer).sendToTarget();
        }

        @Override
        public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onInfo : player = " + mediaPlayer + ", what = " + what + ", extra = " + extra + sn);

            return false;
        }

        @Override
        public void onSeekComplete(MediaPlayer mediaPlayer) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onSeekComplete : player = " + mediaPlayer + sn);
        }

        @Override
        public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
            String sn = " | Thread : " + Thread.currentThread().getName() + " |";
            Log.d(TAG, "onVideoSizeChanged : player = " + mediaPlayer + ", arg2 = " + i + ", arg3 = " + i1 + sn);
        }
    }
}

