package lenovo.multiplayer.advert;

/**
 * @author tengxp
 */
public interface AdConstant {
    String KEY_AD_PLAY = "sys.advert.play";

    String KEY_TV_BOOT_COUNT = "TV_BOOT_COUNT";

    String TAG = "advert";

    String FILE_DIR = "/data/advert/";

    String FILE_BOOT_CFG = "bootad.cfg";

    class Advert {
        int errCode = 0;
        int index;
        int duration;
        String name;
        String path;

        Advert(String name) {
            this.name = name;

            String[] array = name.split("_");
            index = Integer.parseInt(array[0]);
            duration = Integer.parseInt(array[1]);

            path = FILE_DIR + name;
        }
    }
}
