package lenovo.multiplayer.advert;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import lenovo.multiplayer.MultiVideoPlayer;
import lenovo.multiplayer.R;

public class AdvertDialog extends Dialog implements AdConstant {

    public static final int MSG_UPDATE_TEXT = 0x111;

    public static final int INTERVAL_EXIT = 5;

    private TextView mCountText;
    private int interval = 0;
    private boolean startCount;

    private MultiVideoPlayer videoPlayer;
    private List<AdConstant.Advert> mAdvertList;
    private int duration;
    private int validPathLength;
    private int mCurrentPos = -1, mLastDuration;

    public AdvertDialog(Context context) {
        super(context, R.style.DialogFullScreen);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.advert_dialog_main);

        videoPlayer = (MultiVideoPlayer) findViewById(R.id.smart_player);
        videoPlayer.setListener(multiVideoListener);
        videoPlayer.play(getVideoPathArray());

        mCountText = (TextView) findViewById(R.id.count_down);
    }

    private String[] getVideoPathArray() {
        List<AdConstant.Advert> adList = AdUtil.getAdList();
        int size = adList.size();
        String[] array = new String[size];
        for (int i = 0; i < size; i++) {
            AdConstant.Advert advert = adList.get(i);
            array[i] = advert.path;
            interval += advert.duration;
        }
        mAdvertList = adList;
        validPathLength = size;
        return array;
    }

    @Override
    public void dismiss() {
//        Log.w(TAG, "dismiss: ", new Exception());

        if (!isShowing()) {
            return;
        }

        videoPlayer.release();

        AdUtil.recordAdPlay();

        if (mHandler != null) {
            mHandler.removeMessages(MSG_UPDATE_TEXT);
            mHandler = null;
        }

        super.dismiss();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }
        return duration > INTERVAL_EXIT && super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (mCurrentPos != -1) {
            int currentPlayerPosition = (int) (videoPlayer.getCurrentPosition() / 1000f);
            Log.d(TAG, "onBackPressed() : mCurrentPos = [" + mCurrentPos + "], " +
                    "currentPlayerPosition = [" + currentPlayerPosition + "]");
            generateTxtFile(mAdvertList.get(mCurrentPos), currentPlayerPosition);
        }

        super.onBackPressed();
    }

    private void generateTxtFile(final AdConstant.Advert ad, final int currentPosition) {
        new Thread() {
            @Override
            public void run() {
                long now = SystemClock.uptimeMillis();
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(ad.path + ".txt");
                    String txt = String.format("time=%d\nerror=%d", currentPosition == 0 ? ad.duration : currentPosition, ad.errCode);
                    Log.d(TAG, "generateTxtFile: File [" + txt + "]");
                    fos.write(txt.getBytes());
                    fos.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Log.d(TAG, "generateTxtFile: spend time : " + (SystemClock.uptimeMillis() - now));
            }
        }.start();
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            int what = msg.what;
            removeMessages(what);

            try {
                switch (what) {
                    case MSG_UPDATE_TEXT:
                        int currentPosition = videoPlayer.getCurrentPosition();
                        duration = currentPosition / 1000 + mLastDuration;

                        if (interval - duration > 0) {
                            String text = String.format("系统正在启动桌面，剩余%d秒（%s）", interval - duration
                                    , duration < INTERVAL_EXIT ? INTERVAL_EXIT - duration + "秒后可跳过" : "按【返回】跳过");
                            Log.d(TAG, String.format("handleMessage: currentPosition = %d, ", currentPosition) + text);
                            mCountText.setText(text);
                            mCountText.setVisibility(View.VISIBLE);

                            sendEmptyMessageDelayed(MSG_UPDATE_TEXT, 1000);
                        }
                        break;

                    default:
                        mCountText.setVisibility(View.INVISIBLE);
                        break;
                }
            } catch (Exception e) {
                Log.w(TAG, "handleMessage: ", e);
            }
        }
    };

    private MultiVideoPlayer.MultiVideoListener multiVideoListener = new MultiVideoPlayer.MultiVideoListener() {
        @Override
        public void onStartPlay(int pos) {
            mCurrentPos = pos;
            for (int i = 0; i < pos; i++) {
                mLastDuration += mAdvertList.get(i).duration;
            }
            Log.d(TAG, "onStartPlay() : pos = [" + pos + "], duration = [" + mLastDuration + "]");

            if (!startCount) {
                Message.obtain(mHandler, MSG_UPDATE_TEXT).sendToTarget();
                startCount = true;
            }
        }

        @Override
        public void onCompletion(int pos) {
            int size = mAdvertList.size();
            Log.d(TAG, "onCompletion() : pos = [" + pos + "], advert size = [" + size + "]");
            if (pos == -1 || pos >= size) {
                return;
            }

            generateTxtFile(mAdvertList.get(pos), 0);

            verifyDismiss();
        }

        @Override
        public void onError(int pos, int what) {
            int size = mAdvertList.size();
            Log.d(TAG, "onError() : pos = [" + pos + "], advert size = [" + size
                    + "], error = [" + what + "]");
            if (pos > -1 && pos < size) {
                mAdvertList.get(pos).errCode = what;
            }

            verifyDismiss();
        }

        private void verifyDismiss() {
            validPathLength--;
            if (validPathLength == 0) {
                dismiss();
            }
        }
    };
}
