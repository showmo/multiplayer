package lenovo.multiplayer.advert;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AdUtil implements AdConstant {

    private static boolean locked;
    private static boolean debuggable = false;

    public static boolean showAdvert(Context context, DialogInterface.OnDismissListener listener) {
        for (String p : new String[]{"/data/advert", "/data/video"}) {
//            shell("chmod 777 " + p);
            File[] files = new File(p).listFiles();

            int len = files == null ? 0 : files.length;
            for (int i = 0; i < len; i++) {
                File f = files[i];
//                shell("chmod 777 " + f.getPath());
                Log.d(TAG, "showAdvert: path = " + f.getPath());
            }
        }

        boolean isPlay = canPlayAdvert(context);
        if (isPlay || debuggable) {
            locked = true;
            AdvertDialog advertDialog = new AdvertDialog(context);
            if (listener != null) {
                advertDialog.setOnDismissListener(listener);
            }
            advertDialog.show();
        }
        return isPlay;
    }

    public static void setDebuggable(boolean debug) {
        debuggable = debug;
    }

    public static boolean isLocked() {
        return locked;
    }

    public static void recordAdPlay() {
        SystemProperties.set(KEY_AD_PLAY, "true");
        Log.d(TAG, "recordAdPlay() called, duration = " + SystemProperties.getBoolean(KEY_AD_PLAY, false));
        locked = false;
    }

    public static List<Advert> getAdList() {
        ArrayList<Advert> list = new ArrayList<Advert>();

        File cfgFile = new File(FILE_DIR, FILE_BOOT_CFG);
        if (!cfgFile.exists()) {
            Log.w(TAG, "getAdList() called, advert cfgFile not exists.");
            return list;
        } else {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(cfgFile)));
                String name;
                while (!TextUtils.isEmpty(name = reader.readLine())) {
                    Log.d(TAG, "getAdList() called, advert mp4 file : " + name);
                    if (!new File(FILE_DIR, name).exists()) {
                        continue;
                    }

                    list.add(new Advert(name));
                }

                Collections.sort(list, new Comparator<Advert>() {
                    @Override
                    public int compare(Advert o1, Advert o2) {
                        return o1.index - o2.index;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        Log.d(TAG, "getAdList() called, mp4 size : " + list.size());

        return list;
    }

    public static void startFunshionService(Context context) {
        Log.d(TAG, "startFunshionService() : context = [" + context + "]");
        Intent funshionIntent = new Intent("funshion.intent.action.PUBLICITY_UPDATE");
        funshionIntent.setPackage("com.funshion.publicity");
        context.startService(funshionIntent);
    }

    public static void saveTvBootCount(Context context) {
        int bootAdCount = getBootAdCountConfig(context);
        int tvBootCount = getTvBootCount(context);
        int nextCount = (tvBootCount + 1) % bootAdCount;
        context.getSharedPreferences(KEY_TV_BOOT_COUNT, Context.MODE_PRIVATE)
                .edit()
                .putInt(KEY_TV_BOOT_COUNT, nextCount)
                .apply();
        Log.d(TAG, "saveTvBootCount: next tvBootCount = " + nextCount);
    }

    private static boolean canPlayAdvert(Context context) {
        int bootAdCount = getBootAdCountConfig(context);
        int tvBootCount = getTvBootCount(context);
        Log.d(TAG, "canPlayAdvert: adCount = " + bootAdCount + ", tvBootCount = " + tvBootCount);

        if (bootAdCount < 1 || tvBootCount % bootAdCount != 0) {
            return false;
        }

        boolean isPlayed = SystemProperties.getBoolean(KEY_AD_PLAY, false);

        return !isPlayed && getAdList().size() > 0;
    }

    private static int getTvBootCount(Context context) {
        return context.getSharedPreferences(KEY_TV_BOOT_COUNT, Context.MODE_PRIVATE).getInt(KEY_TV_BOOT_COUNT, 0);
    }

    private static int getBootAdCountConfig(Context context) {
        int bootADCount = 1;
        String countStr = Settings.System.getString(context.getContentResolver(), "bootADCount");
        try {
            if (!TextUtils.isEmpty(countStr)) {
                bootADCount = Integer.parseInt(countStr);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return bootADCount;
    }

    private static void shell(String command) {
        try {
            Process exec = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                Log.e(TAG, "advert : " + line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
